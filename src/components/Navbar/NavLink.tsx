import React from 'react'
import { Link } from 'react-router-dom'
type LinkTypeProps = {
    href: string,
    active?: boolean,
    children: React.ReactNode
}
const NavLink: React.FC<LinkTypeProps> = ({ href, active = false, children }) => {
    return (
        <Link to={href} className={(active ? "font-bold " : "font-semibold ") + "text-sm leading-6 text-gray-900"}>
            {children}
        </Link>
    )
}

export default NavLink;
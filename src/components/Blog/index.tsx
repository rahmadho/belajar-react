const posts = [
    {
        id: 1,
        title: 'Boost your conversion rate',
        href: '#',
        description:
            'Illo sint voluptas. Error voluptates culpa eligendi. Hic vel totam vitae illo. Non aliquid explicabo necessitatibus unde. Sed exercitationem placeat consectetur nulla deserunt vel. Iusto corrupti dicta.',
        date: 'Mar 16, 2020',
        datetime: '2020-03-16',
        category: { title: 'Marketing', href: '#' },
        imageUrl: '/images/1.jpg',
        author: {
            name: 'Michael Foster',
            role: 'Co-Founder / CTO',
            href: '#',
            imageUrl:
                'https://i.pravatar.cc/150',
        }
    },
    {
        id: 1,
        title: 'Cras accumsan non purus risus conubia consectetur',
        href: '#',
        description:
            'Aliquam habitant gravida aenean finibus congue litora quisque urna euismod. Eros primis lorem venenatis odio cubilia lacus auctor orci nunc. Cursus mus dolor magna id at vitae aptent duis cubilia quam porttitor.',
        date: 'Mar 16, 2020',
        datetime: '2020-03-16',
        category: { title: 'Sales', href: '#' },
        imageUrl: '/images/2.jpg',
        author: {
            name: 'Michael Foster',
            role: 'Co-Founder / CTO',
            href: '#',
            imageUrl:
                'https://i.pravatar.cc/150',
        },
    },
    {
        id: 1,
        title: 'Id nunc letius fusce sodales neque faucibus suspendisse',
        href: '#',
        description:
            'Semper sodales nec dictumst per blandit. Cubilia accumsan fringilla nisi hac sollicitudin et velit. Himenaeos pretium nunc commodo quis integer mi libero.',
        date: 'Mar 16, 2020',
        datetime: '2020-03-16',
        category: { title: 'Business', href: '#' },
        imageUrl: '/images/3.jpg',
        author: {
            name: 'Michael Foster',
            role: 'Co-Founder / CTO',
            href: '#',
            imageUrl:
                'https://i.pravatar.cc/150',
        },
    }
]

export default function Blog() {
    return (
        <div className="bg-white py-24 sm:py-32">
            <div className="mx-auto max-w-7xl px-6 lg:px-8">
                <div className="mx-auto max-w-2xl lg:text-center">
                    <h2 className="text-base font-semibold leading-7 text-indigo-600">Stay Update</h2>
                    <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                        Berita Terbaru
                    </p>
                </div>
                <div className="mx-auto mt-10 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 lg:mx-0 lg:max-w-none lg:grid-cols-3">
                    {posts.map((post) => (
                        <article key={post.id} className="flex max-w-xl flex-col items-start justify-between">
                            <figure className="mb-6 overflow-hidden rounded-xl">
                                <img src={post.imageUrl} alt="featured image" className="w-full min-h-64 object-cover hover:scale-125 hover:rotate-6 duration-200" />
                            </figure>
                            <div className="flex items-center gap-x-4 text-xs">
                                <time dateTime={post.datetime} className="text-gray-500">
                                    {post.date}
                                </time>
                                <a
                                    href={post.category.href}
                                    className="relative z-10 rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100"
                                >
                                    {post.category.title}
                                </a>
                            </div>
                            <div className="group relative">
                                <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                                    <a href={post.href}>
                                        <span className="absolute inset-0" />
                                        {post.title}
                                    </a>
                                </h3>
                                <p className="mt-5 line-clamp-3 text-sm leading-6 text-gray-600">{post.description}</p>
                            </div>
                            <div className="relative mt-8 flex items-center gap-x-4">
                                <img src={post.author.imageUrl} alt="" className="h-10 w-10 rounded-full bg-gray-50" />
                                <div className="text-sm leading-6">
                                    <p className="font-semibold text-gray-900">
                                        <a href={post.author.href}>
                                            <span className="absolute inset-0" />
                                            {post.author.name}
                                        </a>
                                    </p>
                                    <p className="text-gray-600">{post.author.role}</p>
                                </div>
                            </div>
                        </article>
                    ))}
                </div>
            </div>
        </div>
    )
}

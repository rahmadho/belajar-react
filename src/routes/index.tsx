import { createBrowserRouter } from "react-router-dom";
import Index from "../pages/Index";
import Contact from "../pages/Contact";
import PageNotFound from "../pages/Errors/PageNotFound";
import DetailBlog from "../pages/Blog/detail";

const router = createBrowserRouter([
    {
        path: '/',
        element: <Index />,
        errorElement: <PageNotFound />
    },
    {
        path: '/contact',
        element: <Contact />,
        errorElement: <PageNotFound />
    },
    {
        path: '/blog',
        element: <DetailBlog />,
        errorElement: <PageNotFound />,
        children: [
            {
                path: 'post/:postId',
                element: <DetailBlog />
            }
        ]
    }
])

export {
    router
}
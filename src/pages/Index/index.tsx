import Blog from '../../components/Blog';
// import Feature from '../../components/Feature';
import FeatureWithImage from '../../components/Feature/WithImage';
import Footer from '../../components/Footer';
import Hero from '../../components/Hero';
import Navbar from '../../components/Navbar'
import Service from '../../components/Service';
import Stats from '../../components/Stats';
import Team from '../../components/Team';

function Index() {
    return (
        <>
            <Navbar />
            <Hero />
            <Service />
            {/* <Feature /> */}
            <FeatureWithImage />
            <Blog />
            <Team />
            <Stats />
            <Footer />
        </>
    )
}

export default Index;
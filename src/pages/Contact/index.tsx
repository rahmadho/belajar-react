import Navbar from "../../components/Navbar";
import { Contact as ContactComponent } from "../../components/Contact";
import Footer from "../../components/Footer";
const Contact: React.FC = () => {
    return (
        <>
            <Navbar />
            <ContactComponent />
            <Footer />
        </>
    )
}

export default Contact;
import BlogDetail from "../../components/Blog/read";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";

function DetailBlog() {
    return (
        <>
            <Navbar />
            <BlogDetail />
            <Footer />
        </>
    )
}

export default DetailBlog